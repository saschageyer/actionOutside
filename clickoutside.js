/*
 Version: 1.0.0
  Author: Sascha Geyer
 Website: http://saschageyer.com
    Docs: http://saschageyer.github.io/clickOutside
    Repo: https://github.com/saschageyer/clickOutside
*/

/*! clickOutside v1.0.0 | (c) Sascha Geyer */
(function () {
  'use strict';

  if (!Node.prototype.contains) {
    Node.prototype.contains = function(node) {
      do {
        if (this === node) {
          return true;
        }
      } while (node = node && node.parentNode);
      return false;
    };
  }

  class ClickOutside {
    constructor(elements, callback) {
      this.elements = elements instanceof Array ? elements : [elements];
      this.callback = callback;
      this.evaluateClick = this.evaluateClick.bind(this);
    }

    listen(boolean) {
      if (boolean) {
        document.addEventListener('mouseup', this.evaluateClick);
        document.addEventListener('keyup', this.evaluateClick);       
      } else {
        document.removeEventListener('mouseup', this.evaluateClick);
        document.removeEventListener('keyup', this.evaluateClick);        
      }
    }

    evaluateClick(e) {
      const clickedElement = e.target;
      for (let i = 0; i < this.elements.length; i++) {
        const currentElement = this.elements[i];
        if (clickedElement === currentElement || currentElement.contains(clickedElement)) {
          return null;
        }
      }
      return this.callback();
    }
  }

  if (typeof module === 'object' && typeof exports === 'object') {
    module.exports = ClickOutside;
  }
  else if (typeof define === 'function' && define.amd) {
    define([], ClickOutside);
  }
  else if (typeof exports === 'object') {
    exports.ClickOutside = ClickOutside;
  }
  else {
    window.ClickOutside = ClickOutside;
  }  
})();