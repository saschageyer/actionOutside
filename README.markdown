### --- clickOutside :fire: :fire: :fire: ---
-------
 _Invoke a callback function when clicked or tabbed outside one or multiple DOM elements._

### Sample Usage
```javascript
const clickOutsideNavigation = new ClickOutside(navigation, closeNavigation);
// clickOutsideNavigation.listen(true);
// clickOutsideNavigation.listen(false);
```