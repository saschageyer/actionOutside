const gulp = require('gulp'),
 	babel = require('gulp-babel'),
 	uglify = require('gulp-uglify'),
	rename = require('gulp-rename');

gulp.task('build', () => {
	return gulp.src('clickoutside.js')
	.pipe(babel({presets:['env']}))
	.pipe(uglify())
	.pipe(rename('clickoutside.min.js'))
	.pipe(gulp.dest('./'));
});

gulp.task('copy', () => gulp
 	.src('clickoutside.min.js')
 	.pipe(gulp.dest('docs'))
);

gulp.task('watch', function () {
	gulp.watch('clickoutside.js', ['build', 'copy']);
});

gulp.task('default', ['build', 'copy', 'watch']);
